﻿using Otus.Solid.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid
{
  public class Game
  {
    private ISettings _settings;
    Generator generator;
    private readonly GameUI _gameUI;
    private readonly GameCore _gameCore;

    public Game(ISettings settings)
    {
      _settings = settings;
      generator = new Generator();
      _gameCore = new GameCore(_settings.Min, _settings.Max, generator.Gen(_settings.Min, _settings.Max));
      _gameUI = new GameUI(_settings.Min, _settings.Max);
    }


    public void Run()
    { 
      GameLoop();
    }

    private void GameLoop()
    {
      for (int i = 0; i < _settings.Retry; i++)
      {
        _gameUI.StepByEnd = _settings.Retry - i;
        _gameUI.Print();
        if (int.TryParse(_gameUI.CurrentInputValue, out int inputValue))
        {
          _gameCore.CheckNumber(inputValue);
          if (_gameCore.endRound)
          {
            _gameUI.EndOfGameMessage = $"Поздравляем с победой! Число {_gameUI.CurrentInputValue}";
            _gameUI.Print();
            return; 
          }
          else
          {
            _gameUI.ErrorMessage = _gameCore.message;
          }
        }
        else
        {
          _gameUI.ErrorMessage = "Некорректный ввод";
        }

      }
      _gameUI.EndOfGameMessage = $"Вы проиграли. Попыток не осталось";
      _gameUI.Print();

    }
  }
}
