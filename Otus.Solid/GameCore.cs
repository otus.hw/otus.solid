﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid
{
  public class GameCore 
  {
    int _minValue;
    int _maxValue;
    int _rnd;
    public string message = string.Empty;
    public bool endRound = false;
    public GameCore(int minValue, int maxValue, int rnd)
    {
      _minValue = minValue;
      _maxValue = maxValue;
      _rnd = rnd;
    }
    public void CheckNumber(int number)
    {
      message = "";
      if (number < _minValue || number > _maxValue)
      {
        message = "Некорректный ввод";
        return;
      }
        if (number == _rnd)
        {
          endRound = true;
          message = "Вы выиграли, число " + number;
        }
        else if (number > _rnd)
        {
          message = "Больше";
        }
        else
        {
          message = "Меньше";
        }
    }

  }
}
