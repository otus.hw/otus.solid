﻿using Otus.Solid.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid
{
  public class GameUI : IPrinter, IInput
  {
    public string CurrentInputValue { get; private set; }
    public string ErrorMessage { get; set; } = string.Empty;
    public string EndOfGameMessage { get; set; } = string.Empty;

    public int StepByEnd { get; set; }

    string _title;

    IPrinter _logo;

    public GameUI(int min, int max)
    {
      _title = $"Угадайте число от {min} до {max}";
      _logo = new Logo();
    }


    public void Print()
    {
      Console.Clear();
      _logo.Print();
      if (string.IsNullOrEmpty(EndOfGameMessage))
      {
        Console.WriteLine(_title);
        if (!string.IsNullOrEmpty(ErrorMessage))
        {
          Console.WriteLine(ErrorMessage);
          ErrorMessage = string.Empty;
        }
        Console.WriteLine($"Осталось {StepByEnd} попыток");
        Console.Write("введите число:");
        CurrentInputValue = Input();
      }
      else
      {
        Console.Write(EndOfGameMessage);
        Console.ReadKey();
      }
    }

    public string Input()
    {
      return Console.ReadLine(); 
    }
  }
}
