﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid.Interfaces
{
  public interface IInput
  {
    string Input();
  }
}
