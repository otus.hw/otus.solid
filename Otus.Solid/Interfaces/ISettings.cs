﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid.Interfaces
{
  public interface ISettings
  {
    int Max { get;  }
    int Min { get; }
    int Retry { get; }


  }
}
