﻿using Microsoft.Extensions.Configuration;
using System;

namespace Otus.Solid
{
  class Program
  {
    static void Main(string[] args)
    {
      Game game = new Game(new Settings("appsettings.json"));
      game.Run();
    }
  }
}
