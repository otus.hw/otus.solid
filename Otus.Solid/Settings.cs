﻿using Microsoft.Extensions.Configuration;
using Otus.Solid.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Solid
{
  public class Settings : ISettings
  {
    public int Min { get; private set; }
    public int Max { get; private set; }
    public int Retry { get; private set; }


    public Settings(string file)
    {
      IConfiguration Configuration = new ConfigurationBuilder().AddJsonFile(file, optional: true, reloadOnChange: true).Build();
       Min = Configuration.GetValue<int>("MinValue");
       Max = Configuration.GetValue<int>("MaxValue");
       Retry = Configuration.GetValue<int>("Retry");
    }
  }
}
